<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>

  <meta name="google-site-verification" content="sON0xCzspMFXFfHVpcOX4_Uv7uT0kqXQ0S3gt8tmMRw" />

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>

<?php if ($noindex) { ?>
<!-- OCFilter Start -->
<meta name="robots" content="noindex,nofollow" />
<!-- OCFilter End -->
<?php } ?>
      
  <base href="<?php echo $base; ?>" />

  <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
  <?php } ?>

  <!-- scripts -->
  <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/bootstrap/js/bootstrap.js" type="text/javascript"></script>
  <!-- scripts end -->

  <!-- external -->
  <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
  <!-- external end -->

  <!-- stylesheets -->
  <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
  <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="catalog/view/theme/cream-theme/css/general.css" rel="stylesheet">
  <link href="catalog/view/theme/cream-theme/css/animate.min.css" rel="stylesheet">
  <link href="catalog/view/theme/cream-theme/css/bootstrap-select.min.css" rel="stylesheet">
  <link href="catalog/view/theme/cream-theme/css/lightbox.css" rel="stylesheet">
  <link href="catalog/view/theme/cream-theme/css/owl.transitions.css" rel="stylesheet">
  <link href="catalog/view/theme/cream-theme/css/owl.carousel.css?v=0.0.1" rel="stylesheet">
  <link href="catalog/view/theme/cream-theme/css/rateit.css" rel="stylesheet">
  <link href="catalog/view/theme/cream-theme/css/pink.css" rel="stylesheet">
  <!-- stylesheets end -->

  <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
  <?php } ?>

  <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>

  <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
  <?php } ?>

  <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
  <?php } ?>
</head>
<body class="<?php echo $class; ?>">

<!--===============-->
<!-- mobile header -->
<!--===============-->

<nav class="n-mobile">
  <div class="container">
    <div style="padding-left: 0;" class="col-xs-3">
      <div class="navigate"></div>
    </div>
    <div class="col-xs-5">
      <?php if ($logo) { ?>
        <a href="<?php echo $home; ?>">
          <img style="margin: 0 auto;" src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="mobile-logo img-responsive" width="43" />
        </a>
      <?php } else { ?>
        <h1>
          <a href="<?php echo $home; ?>"><?php echo $name; ?></a>
        </h1>
      <?php } ?>
    </div>
    <button class='mobile-search-button col-xs-2'></button>
    <div class="col-xs-2 top-cart-row js-minicart-container">
      <?= $cart; ?>
    </div>
  </div>
</nav>

<header class="h-mobile">
  <div class="container">
    <ul>
      <li>
        <a href="<?= $account; ?>">
          <i class="icon fa fa-user"></i><?= $text_account; ?>
        </a>
      </li>
      <? if ($categories) : ?>
        <? foreach ($categories as $category) : ?>
            <? if ($category['children']) : ?>
              <li class="dropdown mega-menu has-child">
                <a href="<?= $category['href']; ?>"  data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">
                  <?= $category['name']; ?>
                </a>
                <ul class="dropdown-menu container">
                  <? foreach ($category['children'] as $child) : ?>
                    <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                  <? endforeach; ?>
                </ul>
              </li>
            <? else : ?>
              <li class="dropdown">
                <a href="<?= $category['href']; ?>">
                  <?= $category['name']; ?>
                </a>
              </li>
            <? endif; ?>
        <? endforeach; ?>
      <? endif; ?>
      <? if (isset($informations) && $informations) : ?>
        <? foreach ($informations as $information) : ?>
          <li>
            <a href="<?= $information['href']; ?>">
              <?= $information['title']; ?>
            </a>
          </li>
        <? endforeach; ?>
      <? endif; ?>
        <li>
            <a style="background: #984460;color: #fff;text-align: center;padding: 8px;" href="/index.php?route=product/special">Акции</a>
        </li>
    </ul>
  </div>
</header>

<!--================-->
<!-- desktop header -->
<!--================-->

<header class="h-desktop header-style-1">
  <!-- ============================================== Social networks ============================================== -->
  <div class="container">
    <style>
        ul.social-networks > li, ul.social-networks > li > a {
            width: 36px;
            height: 36px;
            display: inline-block;
            margin-right: 16px;
        }
        ul.social-networks {
            position: absolute;
            left: 50%;
            margin-left: -374px;
            top: 21px;
        }
        @media (min-width: 768px) and (max-width: 991px) {ul.social-networks{position:relative; margin-bottom:37px;text-align:center;margin-left:0;left:auto;}}
        @media (min-width: 992px) and (max-width: 1199px) {ul.social-networks{margin-left:-310px;}}
    </style>
    <ul class="social-networks">
      <li><a style="background-image: url(/image/social/facebook.svg);background-size: 36px;" target="_blank" href="https://www.facebook.com/marina.shibirina" aria-label="Facebook"></a></li>
      <li><a style="background-image: url(/image/social/telegram.svg);background-size: 36px;" target="_blank" href="https://t.me/creamboutique" aria-label="Telegram"></a></li>
      <li><a style="background-image: url(/image/social/youtube.svg);background-size: 36px;" target="_blank" href="https://www.youtube.com/channel/UCc5GcPCCw-JoDx7LBVG27Xw" aria-label="Youtube"></a></li>
      <li><a style="background-image: url(/image/social/instagram.svg);background-size: 36px;" target="_blank" href="https://www.instagram.com/cream_butique/" aria-label="Instagram"></a></li>
    </ul>
  </div>
  <!-- ============================================== Social networks : END ============================================== -->

  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <li>
              <a id="wishlist-total" href="<?= $wishlist; ?>">
                <i class="icon fa fa-heart"></i><span><?= $text_wishlist; ?></span>
              </a>
            </li>
            <!-- <li>
              <a id="compare-total" href="<?= $compare; ?>">
                <i class="icon fa fa-exchange"></i><span><?= $text_compare; ?></span>
              </a>
            </li> -->
            <li>
              <a id="compare-total" href="<?= $shopping_cart; ?>">
                <i class="icon fa fa-gift"></i><span><?= $text_shopping_cart; ?></span>
              </a>
            </li>
            <li><a href="<?=$checkout?>"><i class="icon fa fa-check"></i><?=$text_checkout?></a></li>
            <li><a href="<?=$account?>"><i class="icon fa fa-lock"></i><?=$text_account?></a></li>
          </ul>
        </div>
        <!-- /.cnt-account -->

        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner -->
    </div>
    <!-- /.container -->
  </div>
  <!-- ============================================== TOP MENU : END ============================================== -->

  <div class="main-header">
    <div class="container">
      <div class="row">

        <!-- logo -->
        <div class="col-xs-12 col-sm-12 col-md-2 logo-holder">
          <div class="logo">
            <?php if ($logo) { ?>
              <a href="<?php echo $home; ?>">
                <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" width="140" />
              </a>
            <?php } else { ?>
              <h1>
                <a href="<?php echo $home; ?>"><?php echo $name; ?></a>
              </h1>
            <?php } ?>
          </div>
        </div>

        <!-- search -->
        <div class="col-xs-12 col-sm-9 col-md-8 top-search-holder">
          <?= $search; ?>
        </div>

        <!-- cart-top -->
        <div class="col-xs-12 col-sm-3 col-md-2 animate-dropdown top-cart-row js-minicart-container">
          <?= $cart; ?>
        </div>
      </div>
    </div>
  </div>

  <? if ($categories) { ?>
  <div class="header-nav animate-dropdown">
    <div class="container">
      <div class="yamm navbar navbar-default" role="navigation">
        <div class="nav-bg-class">
          <div class="navbar-collapse">
            <div class="nav-outer">
              <ul class="nav navbar-nav">
                <? foreach ($categories as $category) { ?>
                  <? if ($category['children']) { ?>
                    <li class="dropdown mega-menu">
                      <a href="<?= $category['href']; ?>"  data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">
                        <?= $category['name']; ?>
                      </a>
                      <ul class="dropdown-menu container">
                        <li>
                          <div class="yamm-content">
                            <div class="row">
                              <? foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                <div class="col-xs-12 col-sm-12 col-md-3 col-menu">
                                  <ul class="links">
                                    <?php foreach ($children as $child) { ?>
                                      <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                    <?php } ?>
                                  </ul>
                                </div>
                              <? } ?>
                            </div>
                          </div>
                        </li>
                      </ul>
                  <? } else { ?>
                    <li class="dropdown">
                      <a href="<?= $category['href']; ?>">
                        <?= $category['name']; ?>
                      </a>
                    </li>
                  <? } ?>
                <? } ?>
                <? foreach ($informations as $information) { ?>
                  <li>
                    <a href="<?= $information['href']; ?>">
                      <?= $information['title']; ?>
                    </a>
                  </li>
                <? } ?>
                <style>
                    li#akcii > a.akcii:hover{
                        color:#984460!important;
                    }
                    li#akcii > a.akcii {
                        color: #fff;
                    }
                </style>
                <li id="akcii" style="background-color:#984460;">
                    <a class="akcii" href="/index.php?route=product/special">Акции</a>
                </li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <? } ?>
</header>

<div class="global-wrapper">

  <!-- search -->
  <div class="h-mobile container mobile-search">
    <div class="top-search-holder">
      <?= $search; ?>
    </div>
  </div>

  <div class="container">
    <div id="information"></div>
  </div>
<?php
// Text
$_['text_items']    = 'Товаров %s (%s)';
$_['text_empty']    = 'Ваша коробочка пуста!';
$_['text_cart']     = 'Просмотреть коробочку';
$_['text_checkout'] = 'Оформить заказ';
$_['text_recurring']  = 'Платежный профиль';
$_['text_shopping_cart']  = 'Коробочка';


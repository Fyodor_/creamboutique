jQuery(document).ready(function() {
  'use strict';

  jQuery('.home-owl-carousel').each(function() {
    var owl = $(this);
    var itemPerLine = owl.data('item');
    if (!itemPerLine) {
      itemPerLine = 5;
    }
    owl.owlCarousel({
      items: itemPerLine,
      itemsTablet: [ 768, 2 ],
      navigation: true,
      pagination: false,
      autoplay: true,
      autoplayTimeout: 3000,
      navigationText: [ '', '' ]
    });
  });

  jQuery('.homepage-owl-carousel').each(function() {
    var owl = $(this);
    var itemPerLine = owl.data('item');
    if (!itemPerLine) {
      itemPerLine = 4;
    }
    owl.owlCarousel({
      items: itemPerLine,
      itemsTablet: [ 768, 2 ],
      itemsDesktop: [ 1199, 2 ],
      navigation: true,
      pagination: false,

      navigationText: [ '', '' ]
    });
  });

  jQuery('.blog-slider').owlCarousel({
    items: 2,
    itemsDesktopSmall: [ 979, 2 ],
    itemsDesktop: [ 1199, 2 ],
    navigation: true,
    slideSpeed: 300,
    pagination: false,
    navigationText: [ '', '' ]
  });

  jQuery('.best-seller').owlCarousel({
    items: 4,
    navigation: true,
    itemsDesktopSmall: [ 979, 2 ],
    itemsDesktop: [ 1199, 2 ],
    slideSpeed: 300,
    pagination: false,
    paginationSpeed: 400,
    navigationText: [ '', '' ]
  });

  jQuery('.sidebar-carousel').owlCarousel({
    items: 1,
    itemsTablet: [ 768, 2 ],
    itemsDesktopSmall: [ 979, 2 ],
    itemsDesktop: [ 1199, 1 ],
    navigation: true,
    slideSpeed: 300,
    pagination: false,
    paginationSpeed: 400,
    navigationText: [ '', '' ]
  });

  jQuery('.brand-slider').owlCarousel({
    items: 6,
    navigation: true,
    slideSpeed: 300,
    pagination: false,
    paginationSpeed: 400,
    navigationText: [ '', '' ]
  });
  jQuery('#advertisement').owlCarousel({
    items: 1,
    itemsDesktopSmall: [ 979, 2 ],
    itemsDesktop: [ 1199, 1 ],
    navigation: true,
    slideSpeed: 300,
    pagination: true,
    paginationSpeed: 400,
    navigationText: [ '', '' ]
  });
});

/*===================================================================================*/
/*  LAZY LOAD IMAGES USING ECHO
/*===================================================================================*/
jQuery(function() {
  echo.init({
    offset: 100,
    throttle: 250,
    unload: false
  });
});

/*===================================================================================*/
/*  RATING
/*===================================================================================*/

jQuery(function() {
  jQuery('.rating').rateit({ max: 5, step: 1, value: 4, resetable: false, readonly: true });
});

/*===================================================================================*/
/* PRICE SLIDER
/*===================================================================================*/
jQuery(function() {
  // Price Slider
  if (jQuery('.price-slider').length > 0) {
    jQuery('.price-slider').slider({
      min: 100,
      max: 700,
      step: 10,
      value: [ 200, 500 ],
      handle: 'square'
    });
  }
});

/*===================================================================================*/
/* SINGLE PRODUCT GALLERY
/*===================================================================================*/
jQuery(function() {
  // jQuery('#owl-single-product').owlCarousel({
  //     items:1,
  //     itemsTablet:[768,2],
  //     itemsDesktop : [1199,1],
  // });

  jQuery('#owl-single-product-thumbnails').owlCarousel({
    items: 4,
    pagination: true,
    rewindNav: true,
    itemsTablet: [ 768, 4 ],
    itemsDesktop: [ 1199, 3 ],
    autoPlay: 5000,
    stopOnHover: false
  });

  // jQuery('#owl-single-product2-thumbnails').owlCarousel({
  //     items: 6,
  //     pagination: true,
  //     rewindNav: true,
  //     itemsTablet : [768, 4],
  //     itemsDesktop : [1199,3]
  // });

  // jQuery('.single-product-slider').owlCarousel({
  //     stopOnHover: true,
  //     rewindNav: true,
  //     singleItem: true,
  //     pagination: true
  // });
});

/*===================================================================================*/
/*  WOW
/*===================================================================================*/

jQuery(function() {
  new WOW().init();
});

/*===================================================================================*/
/*  TOOLTIP
/*===================================================================================*/
jQuery("[data-toggle='tooltip']").tooltip();

function viberChat() {
  ScreenWidth = screen.width;
  if (ScreenWidth > 1024) {
    javascript: jivo_api.open();
  } else {
    location.href = 'viber://add?number=380737727978';
  }
}

/* ============================================================================
 * home search
 * ============================================================================ */

jQuery(document).ready(function() {
  jQuery(document).on('click', '.mobile-search-button', function(e) {
    e.preventDefault();
    if (!jQuery('.mobile-search').hasClass('active')) {
      jQuery('html, body').animate({ scrollTop: 0 }, 500);
      jQuery('.mobile-search').toggleClass('active');
    } else jQuery('.mobile-search').toggleClass('active');
  });
});

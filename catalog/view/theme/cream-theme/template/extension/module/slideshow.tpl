<div class="body-content" id="top-banner-and-menu">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
        <div id="hero">
          <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
            <? foreach ($banners as $banner) : ?>
              <div class="item" style="background-image: url(<?=$banner['image'];?>);">
                <div class="container-fluid">
                  <div class="caption bg-color vertical-center text-left">
                    <?php if ($banner['code']) { ?>
                      <?php echo $banner['code'];?>
                    <? } ?>
                  </div>
                  <!-- /.caption -->
                </div>
                <!-- /.container-fluid -->
              </div>
              <!-- /.item -->
            <? endforeach; ?>
          </div>
          <!-- /.owl-carousel -->
        </div>
      </div>
    </div>
</div>
<script>

jQuery(document).ready(function() {
    "use strict";
  /*===================================================================================*/
  /*  OWL CAROUSEL
  /*===================================================================================*/
  jQuery(function () {
      var dragging = true;
      var owlElementID = "#owl-main";

      function fadeInReset() {
          if (!dragging) {
              jQuery(owlElementID + " .caption .fadeIn-1, " + owlElementID + " .caption .fadeIn-2, " + owlElementID + " .caption .fadeIn-3").stop().delay(800).animate({ opacity: 0 }, { duration: 400, easing: "easeInCubic" });
          }
          else {
              jQuery(owlElementID + " .caption .fadeIn-1, " + owlElementID + " .caption .fadeIn-2, " + owlElementID + " .caption .fadeIn-3").css({ opacity: 0 });
          }
      }

      function fadeInDownReset() {
          if (!dragging) {
              jQuery(owlElementID + " .caption .fadeInDown-1, " + owlElementID + " .caption .fadeInDown-2, " + owlElementID + " .caption .fadeInDown-3").stop().delay(800).animate({ opacity: 0, top: "-15px" }, { duration: 400, easing: "easeInCubic" });
          }
          else {
              jQuery(owlElementID + " .caption .fadeInDown-1, " + owlElementID + " .caption .fadeInDown-2, " + owlElementID + " .caption .fadeInDown-3").css({ opacity: 0, top: "-15px" });
          }
      }

      function fadeInUpReset() {
          if (!dragging) {
              jQuery(owlElementID + " .caption .fadeInUp-1, " + owlElementID + " .caption .fadeInUp-2, " + owlElementID + " .caption .fadeInUp-3").stop().delay(800).animate({ opacity: 0, top: "15px" }, { duration: 400, easing: "easeInCubic" });
          }
          else {
              $(owlElementID + " .caption .fadeInUp-1, " + owlElementID + " .caption .fadeInUp-2, " + owlElementID + " .caption .fadeInUp-3").css({ opacity: 0, top: "15px" });
          }
      }

      function fadeInLeftReset() {
          if (!dragging) {
              jQuery(owlElementID + " .caption .fadeInLeft-1, " + owlElementID + " .caption .fadeInLeft-2, " + owlElementID + " .caption .fadeInLeft-3").stop().delay(800).animate({ opacity: 0, left: "15px" }, { duration: 400, easing: "easeInCubic" });
          }
          else {
              jQuery(owlElementID + " .caption .fadeInLeft-1, " + owlElementID + " .caption .fadeInLeft-2, " + owlElementID + " .caption .fadeInLeft-3").css({ opacity: 0, left: "15px" });
          }
      }

      function fadeInRightReset() {
          if (!dragging) {
              jQuery(owlElementID + " .caption .fadeInRight-1, " + owlElementID + " .caption .fadeInRight-2, " + owlElementID + " .caption .fadeInRight-3").stop().delay(800).animate({ opacity: 0, left: "-15px" }, { duration: 400, easing: "easeInCubic" });
          }
          else {
              jQuery(owlElementID + " .caption .fadeInRight-1, " + owlElementID + " .caption .fadeInRight-2, " + owlElementID + " .caption .fadeInRight-3").css({ opacity: 0, left: "-15px" });
          }
      }

      function fadeIn() {
          jQuery(owlElementID + " .active .caption .fadeIn-1").stop().delay(500).animate({ opacity: 1 }, { duration: 1000, easing: "easeOutCubic" });
          jQuery(owlElementID + " .active .caption .fadeIn-2").stop().delay(700).animate({ opacity: 1 }, { duration: 1000, easing: "easeOutCubic" });
          jQuery(owlElementID + " .active .caption .fadeIn-3").stop().delay(1000).animate({ opacity: 1 }, { duration: 1000, easing: "easeOutCubic" });
      }

      function fadeInDown() {
          jQuery(owlElementID + " .active .caption .fadeInDown-1").stop().delay(500).animate({ opacity: 1, top: "0" }, { duration: 1000, easing: "easeOutCubic" });
          jQuery(owlElementID + " .active .caption .fadeInDown-2").stop().delay(700).animate({ opacity: 1, top: "0" }, { duration: 1000, easing: "easeOutCubic" });
          jQuery(owlElementID + " .active .caption .fadeInDown-3").stop().delay(1000).animate({ opacity: 1, top: "0" }, { duration: 1000, easing: "easeOutCubic" });
      }

      function fadeInUp() {
          jQuery(owlElementID + " .active .caption .fadeInUp-1").stop().delay(500).animate({ opacity: 1, top: "0" }, { duration: 1000, easing: "easeOutCubic" });
          jQuery(owlElementID + " .active .caption .fadeInUp-2").stop().delay(700).animate({ opacity: 1, top: "0" }, { duration: 1000, easing: "easeOutCubic" });
          jQuery(owlElementID + " .active .caption .fadeInUp-3").stop().delay(1000).animate({ opacity: 1, top: "0" }, { duration: 1000, easing: "easeOutCubic" });
      }

      function fadeInLeft() {
          jQuery(owlElementID + " .active .caption .fadeInLeft-1").stop().delay(500).animate({ opacity: 1, left: "0" }, { duration: 1000, easing: "easeOutCubic" });
          jQuery(owlElementID + " .active .caption .fadeInLeft-2").stop().delay(700).animate({ opacity: 1, left: "0" }, { duration: 1000, easing: "easeOutCubic" });
          jQuery(owlElementID + " .active .caption .fadeInLeft-3").stop().delay(1000).animate({ opacity: 1, left: "0" }, { duration: 1000, easing: "easeOutCubic" });
      }

      function fadeInRight() {
          jQuery(owlElementID + " .active .caption .fadeInRight-1").stop().delay(500).animate({ opacity: 1, left: "0" }, { duration: 1000, easing: "easeOutCubic" });
          jQuery(owlElementID + " .active .caption .fadeInRight-2").stop().delay(700).animate({ opacity: 1, left: "0" }, { duration: 1000, easing: "easeOutCubic" });
          jQuery(owlElementID + " .active .caption .fadeInRight-3").stop().delay(1000).animate({ opacity: 1, left: "0" }, { duration: 1000, easing: "easeOutCubic" });
      }

      jQuery(owlElementID).owlCarousel({

          autoPlay: 5000,
          smartSpeed: 2500,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          singleItem: true,
          addClassActive: true,
          transitionStyle: "fade",
          navigationText: ["<i class='icon fa fa-angle-left'></i>", "<i class='icon fa fa-angle-right'></i>"],

          afterInit: function() {
              fadeIn();
              fadeInDown();
              fadeInUp();
              fadeInLeft();
              fadeInRight();
          },

          afterMove: function() {
              fadeIn();
              fadeInDown();
              fadeInUp();
              fadeInLeft();
              fadeInRight();
          },

          afterUpdate: function() {
              fadeIn();
              fadeInDown();
              fadeInUp();
              fadeInLeft();
              fadeInRight();
          },

          startDragging: function() {
              dragging = true;
          },

          afterAction: function() {
              fadeInReset();
              fadeInDownReset();
              fadeInUpReset();
              fadeInLeftReset();
              fadeInRightReset();
              dragging = false;
          }

      });

      if (jQuery(owlElementID).hasClass("owl-one-item")) {
          jQuery(owlElementID + ".owl-one-item").data('owlCarousel').destroy();
      }

      jQuery(owlElementID + ".owl-one-item").owlCarousel({
          singleItem: true,
          navigation: false,
          pagination: false
      });
  });

});
</script>

<div id="slideshow<?php echo $module; ?>" class="owl-carousel" style="opacity: 1;">
  <div class="item">
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <?php } ?>
  </div>
</div>
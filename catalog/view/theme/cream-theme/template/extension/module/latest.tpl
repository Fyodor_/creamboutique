<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div id="product-tabs-slider" class="scroll-tabs outer-top-vs">
			<div class="more-info-tab clearfix ">
				<h3 class="new-product-title pull-left"><?php echo $heading_title; ?></h3>
			<!-- /.nav-tabs -->
			</div>
			<div class="tab-content outer-top-xs">
				<div class="tab-pane in active" id="all">
					<div class="product-slider">
						<div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="5">

							<? foreach ($products as $product) : ?>
								<div class="item item-carousel">
									<div class="products">
										<div class="product">
											<div class="product-image">
												<div class="image"><a href="<?=$product['href'];?>"><img src="<?=$product['thumb'];?>" alt=""></a></div>
												<!-- /.image -->
												<? if ($product['price']) : ?>
													<div class="cart b-only-buy clearfix animate-effect">
														<div class="action">
															<ul class="list-unstyled">
																<li class="mini-cart-button add-cart-button btn-group">
																	<button data-toggle="tooltip" class="btn btn-primary icon" type="button" title="<?=$button_cart;?>" onclick="cart.add('<?=$product['product_id'];?>'); return false;" >
																		<i class="fa fa-shopping-cart"></i>
																	</button>
																</li>
																<!-- <li class="mini-wishlist-button add-cart-button btn-group">
																	<button data-toggle="tooltip" class="btn btn-primary icon" title="<?=$button_wishlist;?>" onclick="wishlist.add('<?= $product['product_id']; ?>'); return false;">
																		<i class="icon fa fa-heart"></i>
																	</button>
																</li>
																<li class="mini-compare-button add-cart-button btn-group">
																	<button data-toggle="tooltip" class="btn btn-primary icon" onclick="compare.add('<?php echo $product['product_id']; ?>'); return false;" title="<?=$button_compare;?>">
																		<i class="fa fa-signal" aria-hidden="true"></i>
																	</button>
																</li> -->
															</ul>
														</div>
													  <!-- /.action -->
													</div>
												<? endif; ?>
											</div>

											<div class="product-info text-left">
												<h3 class="name"><a title="<?=$product['name'];?>" href="<?=$product['href'];?>"><?=$product['name'];?></a></h3>

												<div class="description">
													<?=$product['description'];?>
												</div>

												<? if ($product['price']) : ?>
													<div class="product-price">

														<? if (!$product['special']) : ?>
															<span class="price"><?=$product['price'];?></span>
														<? else : ?>
															<span class="price"><?=$product['special'];?></span>
															<span class="price-before-discount"><?=$product['price'];?></span>
														<? endif; ?>

														<? if ($product['tax']) : ?>
															<div class="price-tax"><?=$text_tax;?> <?=$product['tax'];?></div>
														<? endif; ?>

													</div>
												<? else : ?>
													<div class="quantity-container info-container">
													  <div class="form-group row">
													    <div class="col-sm-12">
													      <button onclick="viberChat();" type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary">
													        <i class="fa fa-commenting-o inner-right-vs"></i>
													        Уточнить цену
													      </button>
													    </div>
													  </div>
													</div>
												<? endif; ?>

											</div>

										</div>
									</div>
								</div>
							<? endforeach; ?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
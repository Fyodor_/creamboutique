<div class="js-minicart dropdown dropdown-cart"> <a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
	<div class="items-cart-inner">
	  <div class="basket"> <i class="fa fa-gift"></i> </div>
	  <div class="basket-item-count">
	  	<span class="js-count-products count">
	  		<?= $text_items; ?>
	  	</span>
	  </div>
	  <div class="total-price-basket">
	  	<span class="total-price">
	  		<span class="js-price-text value">
	  			<?= $text_total_cart_price_currency; ?>
	  		</span>
	  	</span>
	  </div>
	</div>
	</a>
	<ul class="dropdown-menu">
	<? if ($products || $vouchers) { ?>
		<? foreach ($products as $product) { ?>
			<li>
				<div class="cart-item product-summary">
					<div class="row">
						<div class="col-xs-4">
							<div class="image">
								<a href="<?= $product['href']; ?>">
									<img src="<?= $product['thumb']; ?>" alt="">
								</a>
							</div>
						</div>
						<div class="col-xs-7">
		          <h3 class="name"><a href="<?= $product['href']; ?>"><?=$product['name'];?></a></h3>
		          <div class="price"><?=$product['total'];?></div>
		        </div>
		        <div class="col-xs-1 action">
		        	<a href="#" onclick="cart.remove('<?=$product['cart_id'];?>'); return false;" title="<?=$button_remove;?>" >
		        		<i class="fa fa-trash"></i>
		        	</a>
		        </div>
					</div>
				</div>
				<div class="clearfix"></div>
        <hr>
			</li>
		<? } ?>
	  <li>
	  	<div class="clearfix cart-total">
	      <div class="pull-right"> <span class="text">Сумма :</span><span class='price'><?=$text_total_cart_price_currency;?></span> </div>
	      <div class="clearfix"></div>
	      <a href="<?=$checkout;?>" class="btn btn-upper btn-primary btn-block m-t-20">
	        <?=$text_checkout;?>
	      </a>
          <a href="<?=$shopping_cart;?>" class="btn btn-upper btn-primary btn-block m-t-20">
            <?=$text_shopping_cart;?>
          </a>
	    </div>
	    <!-- /.cart-total--> 
	  </li>
	<? } else { ?>
			<span><?php echo $text_empty; ?></span>
	<? } ?>
	</ul>
	<!-- /.dropdown-menu--> 
</div>
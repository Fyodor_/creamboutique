<div id="search" class="search-area">
	<form action="index.php">
	  	<div class="control-group">
		  	<? if ( $categories && is_array($categories) && count($categories) > 0 ) : ?>
				<ul class="categories-filter animate-dropdown">
					<li class="dropdown">
						<a class="dropdown-toggle select-category" data-toggle="dropdown" href="#">
							<span class="search-category-text"><?php echo $text_category; ?></span><b class="caret"></b>
						</a>
					    <ul class="dropdown-menu" role="menu" >
					    	<? foreach ($categories as $category_1) : ?>
								<ul class="subcat">
									<li data-value="<?=$category_1['category_id']?>" role="presentation" class="menu-header"><?=$category_1['name'];?></li>
									<? foreach ($category_1['children'] as $category_2) : ?>
										<li data-value="<?=$category_2['category_id'];?>" role="presentation">
											<span role="menuitem" tabindex="-1">
												<?=$category_2['name'];?>
											</span>
										</li>
									<? endforeach; ?>
								</ul>
					    	<? endforeach; ?>
					    	<div class="clearfix"></div>
					    	<hr style="margin:10px 0;">
					    	<li data-value="" role="presentation">
					    		<?=$text_caterogy_all;?>
					    	</li>
					    </ul>
				  </li>
				</ul>
		  	<? endif; ?>
		    <input class="search-field" name="search" value="<?=$search;?>" placeholder="<?=$text_search;?>" autocomplete="off"/>
		    <button class="search-button"></button> 
		</div>
		<input type="hidden" name="route" value="product/search">
		<input type="hidden" name="sub_category" value="1">
		<input type="hidden" name="category_id" value="<?=$category_id;?>">
	</form>
</div>


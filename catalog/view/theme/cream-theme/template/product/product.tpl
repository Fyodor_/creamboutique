<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row single-product">
    <!-- <div class='col-md-3 sidebar'></div> -->
    <div class='col-md-12'>
      <div id="content"><?php echo $content_top; ?>
        <div class="">

          <!-- block 1 -->

          <div class=" fadeInUp detail-block">
            <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
              <div class="product-item-holder size-big single-product-gallery small-gallery">

                <?php if ($thumb || $images) { ?>
                  <div class="thumbnails">
                    <?php if ($thumb) { ?>
                    <div class="product-main-image"><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="catalog/view/theme/cream-theme/images/ajax.gif" data-echo="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></div>
                    <?php } ?>
                    <?php if ($images) { ?>
                    <? $index=0; ?>
                    <div id="owl-single-product-thumbnails">
                      <?php foreach ($images as $image) { ?>
                      <div class="image-additional item<?= $index === 0 ? ' active' : '' ?>">
                        <a class="horizontal-thumb" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>">
                          <img class="img-responsive" src="catalog/view/theme/cream-theme/images/ajax.gif" data-echo="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                        </a>
                      </div>
                      <? $index++; ?>
                      <?php } ?>
                    </div>
                    <?php } ?>
                  </div>
                <?php } ?>

                <? if ($advantage) : ?>
                  <?=$advantage;?>
                <? endif; ?>

              </div>
            </div>
            <div class="col-sm-6 col-md-7 product-info product-info-block">
              <h1 class="name"><?php echo $heading_title; ?></h1>
              <? if ( $weight ) : ?>
                <p><?=$weight;?></p>
              <? endif; ?>
              <p><?php echo $description; ?></p>
              <ul class="list-unstyled stock-container">
                <?php if ($review_status) { ?>
                <li class="custom-rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($rating < $i) { ?>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } else { ?>
                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } ?>
                  <?php } ?>
                    <a href="#" onclick="$('a[href=\'#tab-review\']').trigger('click'); $('html,body').animate({scrollTop: $('a[href=\'#tab-review\']').offset().top},'slow'); return false;"><?php echo $reviews; ?></a> / <a href="#" onclick="$('a[href=\'#tab-review\']').trigger('click'); $('html,body').animate({scrollTop: $('a[href=\'#tab-review\']').offset().top},'slow'); return false;"><?php echo $text_write; ?></a>
                </li>
                <?php } ?>
                <?php if ($manufacturer) { ?>
                  <li class="stock-box"><span class="label"><?php echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
                <?php } ?>
                <?php if ($reward) { ?>
                  <li class="stock-box"><span class="label"><?php echo $text_reward; ?></span> <span class="value"><?php echo $reward; ?></span></li>
                <?php } ?>
                <li class="stock-box"><span class="label"><?php echo $text_stock; ?></span> <span class="value"><?php echo $stock; ?></span></li>
              </ul>
              <div class="row price-container info-container m-t-20">
                  <?php if ($price) { ?>
                    <div class="col-sm-6 price-box">
                      <ul class="list-unstyled">
                    <?php if (!$special) { ?>
                      <li>
                        <span class="price"><?php echo $price; ?></span>
                      </li>
                    <?php } else { ?>
                      <li><span class="price"><?php echo $special; ?></span> <span class="price-strike"><?php echo $price; ?></span></li>
                    <?php } ?>
                    <?php if ($tax) { ?>
                    <li><?php //echo $text_tax; ?> <?php //echo $tax; ?></li>
                    <?php } ?>
                    <?php if ($points) { ?>
                    <li><?php echo $text_points; ?> <?php echo $points; ?></li>
                    <?php } ?>
                    <?php if ($discounts) { ?>
                    <li>
                      <hr>
                    </li>
                    <?php foreach ($discounts as $discount) { ?>
                    <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
                    <?php } ?>
                    <?php } ?>
                      </ul>
                    </div>
                  <?php } else { ?>
                    <div class="col-sm-12 price-box">
                      <ul class="list-unstyled">
                    <li>
                      <span class="price">Производитель скрыл цену</span>
                    </li>
                      </ul>
                    </div>
                  <?php } ?>
                <?php if ($price) { ?>
                  <div class="col-sm-6">
                    <div class="btn-group favorite-button m-t-10">
                      <button type="button" data-toggle="tooltip" class="btn btn-primary" data-placement="bottom" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i></button>
                    </div>
                  </div>
                <?php } ?>
              </div>

              <?php if ($price) { ?>
                <div id="product">
                  <?php if ($options) { ?>
                  <hr>
                  <h3><?php echo $text_option; ?></h3>
                  <?php foreach ($options as $option) { ?>
                  <?php if ($option['type'] == 'select') { ?>
                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                    <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                      <option value=""><?php echo $text_select; ?></option>
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                      <?php if ($option_value['price']) { ?>
                      (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                      <?php } ?>
                      </option>
                      <?php } ?>
                    </select>
                  </div>
                  <?php } ?>
                  <?php if ($option['type'] == 'radio') { ?>
                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                    <label class="control-label"><?php echo $option['name']; ?></label>
                    <div id="input-option<?php echo $option['product_option_id']; ?>">
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <div class="radio">
                        <label>
                          <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                          <?php if ($option_value['image']) { ?>
                          <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                          <?php } ?>
                          <?php echo $option_value['name']; ?>
                          <?php if ($option_value['price']) { ?>
                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                          <?php } ?>
                        </label>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                  <?php } ?>
                  <?php if ($option['type'] == 'checkbox') { ?>
                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                    <label class="control-label"><?php echo $option['name']; ?></label>
                    <div id="input-option<?php echo $option['product_option_id']; ?>">
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                          <?php if ($option_value['image']) { ?>
                          <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                          <?php } ?>
                          <?php echo $option_value['name']; ?>
                          <?php if ($option_value['price']) { ?>
                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                          <?php } ?>
                        </label>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                  <?php } ?>
                  <?php if ($option['type'] == 'text') { ?>
                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                  </div>
                  <?php } ?>
                  <?php if ($option['type'] == 'textarea') { ?>
                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                    <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                  </div>
                  <?php } ?>
                  <?php if ($option['type'] == 'file') { ?>
                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                    <label class="control-label"><?php echo $option['name']; ?></label>
                    <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                    <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                  </div>
                  <?php } ?>
                  <?php if ($option['type'] == 'date') { ?>
                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                    <div class="input-group date">
                      <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                      <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                      </span></div>
                  </div>
                  <?php } ?>
                  <?php if ($option['type'] == 'datetime') { ?>
                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                    <div class="input-group datetime">
                      <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                      <span class="input-group-btn">
                      <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                      </span></div>
                  </div>
                  <?php } ?>
                  <?php if ($option['type'] == 'time') { ?>
                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                    <div class="input-group time">
                      <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                      <span class="input-group-btn">
                      <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                      </span></div>
                  </div>
                  <?php } ?>
                  <?php } ?>
                  <?php } ?>
                  <?php if ($recurrings) { ?>
                  <hr>
                  <h3><?php echo $text_payment_recurring; ?></h3>
                  <div class="form-group required">
                    <select name="recurring_id" class="form-control">
                      <option value=""><?php echo $text_select; ?></option>
                      <?php foreach ($recurrings as $recurring) { ?>
                      <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                      <?php } ?>
                    </select>
                    <div class="help-block" id="recurring-description"></div>
                  </div>
                  <?php } ?>
                  <div class="quantity-container info-container">
                    <div class="form-group row">
                      <div class="col-sm-2">
                        <select name="quantity" id="input-quantity" class="form-control">
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                          <option value="10">10</option>
                        </select>
                        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                      </div>
                      <div class="col-sm-8">
                        <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary">
                          <i class="fa fa-shopping-cart inner-right-vs"></i>
                          <?php echo $button_cart; ?>
                        </button>
                        <? if ( strtolower($manufacturer) === 'medik8' ) : ?>
                          <button style="    background: #984460;color: #fff;" onclick="alert(1);" type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary">
                            <i class="fa fa-commenting-o inner-right-vs"></i>
                            Получить скидку
                          </button>
                        <? endif; ?>
                      </div>
                    </div>
                  </div>
                  <?php if ($minimum > 1) { ?>
                  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                  <?php } ?>
                </div>

              <?php } else {?>
                <div class="quantity-container info-container">
                  <div class="form-group row">
                    <div class="col-sm-5">
                      <button onclick="viberChat();" type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary">
                      <!-- onclick="javascript:jivo_api.open();" -->
                        <i class="fa fa-commenting-o inner-right-vs"></i>
                        Уточнить цену в чате
                      </button>
                    </div>
                  </div>
                </div>
              <?php } ?>

            </div>
          </div>

          <!-- end block 1 -->

          <div class="product-tabs inner-bottom-xs  fadeInUp">
            <div class="row">

              <div class="col-sm-3">
                <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                  <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
                  <?php if ($attribute_groups) { ?>
                  <!-- <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li> -->
                  <?php } ?>
                  <?php if ($review_status) { ?>
                  <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
                  <?php } ?>
                </ul>
              </div>

              <div class="col-sm-9">
                <div class="tab-content">
                  <div class="tab-pane active" id="tab-description">
                    <?php if ($attribute_groups) { ?>
                    <div class="tab-pane" id="tab-specification">
                      <table class="table table-bordered">
                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                        <thead>
                          <tr>
                            <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                          <tr>
                            <td><?php echo $attribute['name']; ?></td>
                            <td><?php echo $attribute['text']; ?></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                        <?php } ?>
                      </table>
                    </div>
                    <?php } ?>
                  </div>
                  <?php if ($review_status) { ?>
                  <div class="tab-pane" id="tab-review">
                    <form class="form-horizontal" id="form-review">
                      <div id="review"></div>
                      <h2><?php echo $text_write; ?></h2>
                      <?php if ($review_guest) { ?>
                      <div class="form-group required">
                        <div class="col-sm-12">
                          <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                          <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group required">
                        <div class="col-sm-12">
                          <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                          <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                          <div class="help-block"><?php echo $text_note; ?></div>
                        </div>
                      </div>
                      <div class="form-group required">
                        <div class="col-sm-12">
                          <label class="control-label"><?php echo $entry_rating; ?></label>
                          &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                          <input type="radio" name="rating" value="1" />
                          &nbsp;
                          <input type="radio" name="rating" value="2" />
                          &nbsp;
                          <input type="radio" name="rating" value="3" />
                          &nbsp;
                          <input type="radio" name="rating" value="4" />
                          &nbsp;
                          <input type="radio" name="rating" value="5" />
                          &nbsp;<?php echo $entry_good; ?></div>
                      </div>
                      <?php echo $captcha; ?>
                      <div class="buttons clearfix">
                        <div class="pull-right">
                          <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                        </div>
                      </div>
                      <?php } else { ?>
                      <?php echo $text_login; ?>
                      <?php } ?>
                    </form>
                  </div>
                  <?php } ?>
                </div>
              </div>

            </div>
          </div> <!-- product-tabs -->
        </div>

        <!-- recommendations -->

        <section class="section featured-product  fadeInUp">
          <?php if ($products) { ?>
            <h3 class="section-title"><?php echo $text_related; ?></h3>
            <div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">
              <?php $i = 0; ?>
              <?php foreach ($products as $product) { ?>
              <div class="item item-carousel">
                <div class="products">
                  <div class="product">
                    <div class="product-image">
                      <div class="image">
                        <a href="<?php echo $product['href']; ?>">
                          <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                        </a>
                      </div>
                      <div class="cart clearfix animate-effect">
                        <div class="action">
                          <ul class="list-unstyled">
                            <li class="mini-cart-button add-cart-button btn-group">
                              <button type="button" class="btn btn-primary icon" title="<?=$button_cart;?>" data-toggle="tooltip" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
                                <i class="fa fa-shopping-cart"></i>
                              </button>
                            </li>
                            <li class="mini-wishlist-button add-cart-button btn-group">
                              <button type="button" class="btn btn-primary icon" title="<?=$button_wishlist;?>" data-toggle="tooltip" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                                <i class="icon fa fa-heart"></i>
                              </button>
                            </li>
                            <li class="mini-compare-button add-cart-button btn-group">
                              <button type="button" class="btn btn-primary icon" title="<?=$button_compare;?>" data-toggle="tooltip" onclick="compare.add('<?php echo $product['product_id']; ?>');">
                                <i class="fa fa-signal"></i>
                              </button>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="product-info text-left">
                      <h3 class="name">
                        <a href="<?php echo $product['href']; ?>">
                          <?php echo $product['name']; ?>
                        </a>
                      </h3>
                      <?php if ($product['price']) { ?>
                        <div class="product-price">
                          <?php if (!$product['special']) { ?>
                            <span class="price"><?php echo $product['price']; ?></span>
                          <?php } else { ?>
                            <span class="price"><?php echo $product['special']; ?></span>
                            <span class="price-before-discount"><?php echo $product['price']; ?></span>
                          <?php } ?>
                        </div>
                      <? } ?>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          <?php } ?>
        </section>

        <!-- recommendations end -->

        <?php if ($tags) { ?>
        <p><?php echo $text_tags; ?>
          <?php for ($i = 0; $i < count($tags); $i++) { ?>
          <?php if ($i < (count($tags) - 1)) { ?>
          <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
          <?php } else { ?>
          <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
          <?php } ?>
          <?php } ?>
        </p>
        <?php } ?>
        <?php echo $content_bottom; ?>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'number\'], #product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
        $('#information').html('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

        // Need to set timeout otherwise it wont update the total
        setTimeout(function () {
          $('.js-minicart').find('.js-price-text').html(json['total']);
          $('.js-minicart').find('.js-count-products').html(json['count']);
        }, 100);

        $('html, body').animate({ scrollTop: 0 }, 'slow');

        $('.js-minicart-container').load('index.php?route=common/cart/info');
      }
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>

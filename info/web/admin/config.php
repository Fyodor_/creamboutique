<?php
// HTTP
define('HTTP_SERVER', 'https://cream.boutique/admin/');
define('HTTP_CATALOG', 'https://cream.boutique/');

// HTTPS
define('HTTPS_SERVER', 'https://cream.boutique/admin/');
define('HTTPS_CATALOG', 'https://cream.boutique/');

// DIR
define('DIR_APPLICATION', '/home/koscn106/test/admin/');
define('DIR_SYSTEM', '/home/koscn106/test/system/');
define('DIR_IMAGE', '/home/koscn106/test/image/');
define('DIR_LANGUAGE', '/home/koscn106/test/admin/language/');
define('DIR_TEMPLATE', '/home/koscn106/test/admin/view/template/');
define('DIR_CONFIG', '/home/koscn106/test/system/config/');
define('DIR_CACHE', '/home/koscn106/test/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/koscn106/test/system/storage/download/');
define('DIR_LOGS', '/home/koscn106/test/system/storage/logs/');
define('DIR_MODIFICATION', '/home/koscn106/test/system/storage/modification/');
define('DIR_UPLOAD', '/home/koscn106/test/system/storage/upload/');
define('DIR_CATALOG', '/home/koscn106/test/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'koscn106_marina');
define('DB_PASSWORD', 'mU-[ES}Wwg[(');
define('DB_DATABASE', 'koscn106_test');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
